export default function GetObjectKeys(obj: object): Array<string> {
  return Object.keys(obj);
}